import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { UserService } from '../_services/user.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private userService: UserService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap({
        next: (event: any) => {
          if (event instanceof HttpResponse) {
            if ([401, 403].indexOf(event.status) !== -1) {
              // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
              this.userService.logout();
            }
          }
          return event
        },
        error: (error: any) => {
          console.log([401, 403].indexOf(error.status) !== -1);
          if ([401, 403].indexOf(error.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.userService.logout();
          }
        },
      })
    );

    // return next.handle(request).pipe(catchError((err) => throwError(err.error)));
  }
}
