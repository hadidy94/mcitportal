import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map, tap, shareReplay } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { JwtService } from './jwt.service';
import { LoginUser, User } from '../_interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private currentUserSubject = new BehaviorSubject<User | null>(null);
  public currentUser = this.currentUserSubject
    .asObservable()
    .pipe(distinctUntilChanged());

  public isAuthenticated = this.currentUser.pipe(map((user) => !!user));

  constructor(
    private router: Router,
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  login(credentials: {
    userNameOrEmailAddress: string;
    password: string;
  }): Observable<any> {
    return this.http
      .post<any>(
        `http://mcit-intapps01:2030/api/TokenAuth/Authenticate`,
        credentials
      )
      .pipe(
        tap((user) => {
          // this.currentUserSubject.next(user);
          this.setAuth(user.result.accessToken);
        })
      );
  }

  getUserData(): Observable<any> {
    return this.http
      .get<any>(`${environment.apiUrl}/UserProfile/GetCurrentUserDetails`)
      .pipe(
        map((res) => {
          this.currentUserSubject.next(res.result)
        }),
        shareReplay(1)
      );
  }

  getUserDataById(id: string) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/UserProfile/GetUserById?id=${id}`
    );
  }

  setAuth(token: string): void {
    this.jwtService.saveToken(token);
    this.getUserData()
  }

  logout() {
    // remove user from local storage to log user out
    this.purgeAuth();
    this.router.navigate(['/']);
  }

  purgeAuth(): void {
    this.jwtService.destroyToken();
    this.currentUserSubject.next(null);
  }
}
