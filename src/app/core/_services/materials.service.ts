import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map,shareReplay, take,first } from 'rxjs/operators'
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MaterialsService {
  constructor(private http: HttpClient) {}

  getAllMaterials(
    selectedMaterials: any,
    pageNum: number,
    sortingValue: string,
    myFiles: boolean,
    selectedYear: number
    
  ): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/KnowledgeMaterial/GetAll?CategoryIds=${selectedMaterials}&Sorting=${sortingValue}&MyFiles=${myFiles}&SkipCount=${pageNum}&Year=${selectedYear}`
    ).pipe(
      first(),
      map((res: any) => {
        return res.result
      }),
      shareReplay(1)
    );
  }
}
