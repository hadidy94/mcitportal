import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'customDate',
})
export class CustomDatePipe extends DatePipe implements PipeTransform {
  override transform(value: any, args?: any): any {
    return this.DateFormate(value);
  }

  DateFormate(dateVal: string) {
    const months = [
      'يناير',
      'فبراير',
      'مارس',
      'إبريل',
      'مايو',
      'يونيو',
      'يوليو',
      'أغسطس',
      'سبتمبر',
      'أكتوبر',
      'نوفمبر',
      'ديسمبر',
    ];
    const date = new Date(dateVal);
    const delDateString =
      date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
    return delDateString;
  }

}
