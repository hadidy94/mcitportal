interface UserMainData {
  id: number,
  userName: string,
  name: string,
  surname: string,
  emailAddress: string,
  isActive: boolean,
  fullName: string,
  lastLoginTime: string,
  creationTime: string,
  roleNames: string[]
}

interface UserDetails {
  id: number,
  userName: string,
  firstName: string,
  surName: string,
  emailAddress: string,
  gender: string,
  userImagePath: string,
  phoneNumber: string,
  userJobTitle: string,
  userExtention: string,
  officeNumber: string,
  userDepartment: string,
  userAgency: string,
  userId: number
}

interface UserItemsCount {
  userId: number,
  materialsCount: number,
  blogsCount: number,
  sessionsCount: number,
  answersCount: number,
  questionsCount: number,
  ideasCount: number
}

export interface User {
  user: UserMainData,
  userDetails: UserDetails,
  userItemsCount: UserItemsCount
}

export interface LoginUser {
  accessToken: string,
  encryptedAccessToken: string,
  expireInSeconds: number,
  userId: number,
  email: string,
  fullName: string,
  phone: string,
  roles: string[]
}
