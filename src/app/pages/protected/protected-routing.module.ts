import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MaterialsComponent } from './materials/materials.component';
import { SingleMaterialComponent } from './materials/single-material/single-material.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'materials',
    component: MaterialsComponent
  }
  ,
  {
    path: 'materials/:id',
    component: SingleMaterialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
