import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-materials-filter',
  templateUrl: './materials-filter.component.html',
  styleUrls: ['./materials-filter.component.scss']
})
export class MaterialsFilterComponent {
  @Output() selectedValue = new EventEmitter<string>();
  selectedProvince = null;


  checkOptionsOne = [
    { label: 'Apple', value: 'Apple', checked: true },
    { label: 'Pear', value: 'Pear', checked: true },
    { label: 'Orange', value: 'Orange', checked: true }
  ];

  provinceChange($event: string) {
    this.selectedValue.emit($event);
  }

  saveValue($event: any){
    console.log($event)
  }






}
