import { Component, SimpleChanges } from '@angular/core';
import { MaterialsService } from 'src/app/core/_services/materials.service';
import { TranslateService } from '@ngx-translate/core';
// import { DateFormate } from 'src/app/core/_helpers/constant/dateFormate';
import { interval } from 'rxjs';


@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss'],
})
export class MaterialsComponent {
  isLoadingMaterial: boolean = false
  selectedMaterials = '';
  pageNum = 1;
  sorting = 'desc';
  myFiles = true;
  selectedYear = 0;
  totalCount!: number;

  // model
  isVisible = false;

  materialData: any;
  constructor(
    private materialsService: MaterialsService,
    public translate: TranslateService
    ) {}

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    console.log('changes')

  }

  ngOnInit(): void {
    this.getMaterialData();
  }

  getMaterialData() {
    this.isLoadingMaterial = true
    const ss = this.materialsService
      .getAllMaterials(
        this.selectedMaterials,
        this.pageNum,
        this.sorting,
        this.myFiles,
        this.selectedYear
      )
      .subscribe((el) => {
        console.log(el);
        this.materialData = el
        this.pageNum = el.currentPage;
        this.totalCount = el.result.totalCount;
        this.isLoadingMaterial = false
      });
  }



  onPageIndexChange($event: any) {
    this.pageNum = $event;
    this.getMaterialData();
    // console.log($event);
  }

  addBook($event: any) {
    this.sorting = $event;
    this.getMaterialData();
  }


  showModal(): void {
    this.isVisible = true;
  }

  closeModel(): void {
    this.isVisible = false;
  }


}
