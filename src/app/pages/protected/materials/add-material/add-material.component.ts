import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-add-material',
  templateUrl: './add-material.component.html',
  styleUrls: ['./add-material.component.scss']
})
export class AddMaterialComponent {
@Input() isVisible!: boolean;
@Output() showValue = new EventEmitter<boolean>();

handleOk(): void {
  console.log('Button ok clicked!');
  this.isVisible = false;
  this.showValue.emit(false)
}

handleCancel(): void {
  console.log('Button cancel clicked!');
  this.isVisible = false;
  this.showValue.emit(false)
}
}
