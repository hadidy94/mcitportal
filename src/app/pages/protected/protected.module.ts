import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProtectedRoutingModule } from './protected-routing.module';
import { HomeComponent } from './home/home.component';
import { MaterialsComponent } from './materials/materials.component';
import { MaterialsFilterComponent } from './materials/materials-filter/materials-filter.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SingleMaterialComponent } from './materials/single-material/single-material.component';
import { MultiSelectComponent } from 'src/app/ui/components/multi-select/multi-select.component';
import { AddMaterialComponent } from './materials/add-material/add-material.component';

@NgModule({
  declarations: [
    HomeComponent,
    MaterialsComponent,
    MaterialsFilterComponent,
    SingleMaterialComponent,
    MultiSelectComponent,
    AddMaterialComponent
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    SharedModule
  ],
  bootstrap: [
  ]
})
export class ProtectedModule { }
