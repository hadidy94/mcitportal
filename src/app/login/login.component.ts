import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { UserService } from '../core/_services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm!: FormGroup;
  isLoading = false;
  submitted = false;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {
    if (this.userService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userNameOrEmailAddress: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isLoading = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.userService
      .login({
        userNameOrEmailAddress: this.f['userNameOrEmailAddress'].value,
        password: this.f['password'].value,
      })
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from query parameters or default to home page
          this.userService.getUserData().subscribe((el) => {
            this.isLoading = false;
            const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
            this.router.navigateByUrl(returnUrl);
          })
        },
        error: (error: any) => {
          this.error = error;
          this.isLoading = false;
        },
      });

    // console.log(this.loginForm.value);
  }
}
