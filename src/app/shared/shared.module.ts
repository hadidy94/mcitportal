import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzModalModule } from 'ng-zorro-antd/modal';

import { MainLayoutComponent } from './main-layout/main-layout.component';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { RouterModule } from '@angular/router';


import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';

import { CustomDatePipe } from 'src/app/core/pipe/custom.datepipe';


@NgModule({
  declarations: [
    MainLayoutComponent,
    DashboardLayoutComponent,
    CustomDatePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule,
    NzButtonModule,
    NzPaginationModule,
    NzSelectModule,
    NzSkeletonModule,
    NzEmptyModule,
    NzCheckboxModule,
    NzModalModule,
    RouterModule.forChild([])
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule,
    NzButtonModule,
    NzPaginationModule,
    MainLayoutComponent,
    NzSelectModule,
    NzSkeletonModule,
    NzEmptyModule,
    NzCheckboxModule,
    NzModalModule,
    DashboardLayoutComponent,
    CustomDatePipe
  ]
})
export class SharedModule { }
