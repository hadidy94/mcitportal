import { Component, Inject } from '@angular/core';
import { UserService } from 'src/app/core/_services';
import { User } from 'src/app/core/_interfaces/user';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from "@angular/common";


@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent {
  userData!: User | null;
  destroy$ = new Subject<void>();
  constructor(
    private userService: UserService,
    public translate: TranslateService,
    @Inject(DOCUMENT) private document: Document ) {}

  ngOnInit(): void {
    this.userData = this.userService.currentUserValue;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  switchLang(lang: string) {
    let htmlTag = this.document.getElementsByTagName("html")[0] as HTMLHtmlElement;
    htmlTag.dir = lang === "ar" ? "rtl" : "ltr";
    this.translate.use(lang);
    this.changeCssFile(lang);

  }

  changeCssFile(lang: string) {
    let headTag = this.document.getElementsByTagName("head")[0] as HTMLHeadElement;
    let existingLink = this.document.getElementById("langCss") as HTMLLinkElement;

    let bundleName = lang === "ar" ?  "arabicStyle.scss":"englishStyle.scss";

    if (existingLink) {
      existingLink.href = bundleName;
    } else {
      let newLink = this.document.createElement("link");
      newLink.rel = "stylesheet";
      newLink.type = "text/css";
      newLink.id = "langCss";
      newLink.href = bundleName;
      headTag.appendChild(newLink);
    }

  }
}
