import { Component  } from '@angular/core';
import { UserService } from './core/_services/user.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: 'app-root',
  // styleUrls: ['./app.component.scss'],
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
  title = 'mcitPortal';

  constructor(
    public translate: TranslateService
    ) {
    translate.addLangs(['en', 'ar']);
    translate.setDefaultLang('ar');
  }


}
