import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { AuthGuard } from './core/_helpers/auth.guard';
import { MainLayoutComponent } from './shared/main-layout/main-layout.component';
import { DashboardLayoutComponent } from './shared/dashboard-layout/dashboard-layout.component';


const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/protected/protected.module').then(h =>h.ProtectedModule),
  },
  {
    path: 'admin',
    component: DashboardLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      isAdminRole: "SuperAdmin"
    },
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(h =>h.DashboardModule),
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
