import {
  Component,
  Input,
  EventEmitter,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss'],
})
export class MultiSelectComponent {
  @Input() categories!: Array<any>;
  @Output() checkedValues = new EventEmitter<[any]>();

  allChecked = true;
  indeterminate = false;
  // checkedValues: any;

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (changes) {
      console.log(changes['categories'].currentValue);
    }
  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.generateCheckValues();
  }

  updateAllChecked(): void {
    this.indeterminate = false;
    if (this.allChecked) {
      this.categories = this.categories.map((item) => ({
        ...item,
        checked: true,
      }));
    } else {
      this.categories = this.categories.map((item) => ({
        ...item,
        checked: false,
      }));
    }
    this.generateCheckValues();
  }

  updateSingleChecked(): void {
    if (this.categories.every((item) => !item.checked)) {
      this.allChecked = false;
      this.indeterminate = false;
    } else if (this.categories.every((item) => item.checked)) {
      this.allChecked = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
    }

    this.generateCheckValues();
  }

  generateCheckValues() {
    const listedValues: any = this.categories
      .filter((el) => el.checked)
      .map((el) => el.value);
    this.checkedValues.emit(listedValues);
  }
}
